# Interviews

This repo is a private preparation for interviews. If you see it useful, please give me a star

## Data Structure and Algorithm

## Devops

### Kubernetes

### CI

- Jenkins
- CI service: CircleCI, Travis, Gitlab CI

### Cloud Platform

- AWS
- AZ
- GCP

- Service Alias

### Tools

- Monitor

  - New Relic
  - Splunk

- Security

  - Cylance
  - McAfee Cloud

- Infrastructure as Code

  - Cloudformation
  - Terraform

<!-- ## Devops -->

## Backend

### System Design Primer

- https://github.com/donnemartin/system-design-primer

### Optimization

- Database
  - N+1 query problem
  - Indexing
  - Normalize and Denormalize
- Caching
  - Invalidate cache strategy

### Zero downtime

- https://itnext.io/creating-a-zero-downtime-cluster-in-nodejs-ad879ee3160

### Nodejs

- [Awesome Nodejs](https://github.com/sindresorhus/awesome-nodejs)
- [The javascript language](https://javascript.info/js)

#### Async

- http://farzicoder.com/Callback-Hell-Promises-generators-Async-Await-in-Node-js-Javascript/

#### Data Types

- [Data types](https://javascript.info/data-types)
- [Map vs WeakMap](https://www.tutorialspoint.com/what-is-the-difference-between-map-and-weakmap-in-javascript)
- [Truthy](https://developer.mozilla.org/en-US/docs/Glossary/Truthy) vs [Falsy](https://developer.mozilla.org/en-US/docs/Glossary/Falsy)
- [Null vs Undefined](https://codeburst.io/javascript-null-vs-undefined-20f955215a2)
- [Symbol](https://javascript.info/symbol)
- [Iterators and Generators](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators)

#### Functional Programming (FP)

- Pure Function
- Immutability

- https://www.freecodecamp.org/news/functional-programming-principles-in-javascript-1b8fc6c3563f/
- https://medium.com/javascript-scene/master-the-javascript-interview-what-is-functional-programming-7f218c68b3a0

#### Clean Code

- https://github.com/ryanmcdermott/clean-code-javascript

#### Books

- https://github.com/getify/You-Dont-Know-JS

#### Typescript

- Decorator, Reflector (Decorator pattern, runtime)

#### Concurrency and Parallelism

- Cluster mode
- Multiple instance

<!-- ### Nodejs -->

### Probabilistic data structure

- [Bloom Filter](https://github.com/jwasham/coding-interview-university#bloom-filter)
- [HyperLogLog](https://github.com/jwasham/coding-interview-university#hyperloglog)
  - https://engineering.fb.com/data-infrastructure/hyperloglog/

### Benchmark tools

## Terminal

- Oh my ZSH
  - auto suggesstion, hightlight, git, powerline

## Movies

And somethings you need to be relax

- https://www.rottentomatoes.com/m/ford_v_ferrari

## Design

- UML
- Diagram

## Misc

- Tensorflow: Try a new thing.
  - https://nguyenvanhieu.vn/khoa-hoc-tensorflow/
